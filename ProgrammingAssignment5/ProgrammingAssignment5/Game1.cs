using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using TeddyMineExplosion;

namespace ProgrammingAssignment5
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int WINDOW_WIDTH = 800;
        const int WINDOW_HEIGHT = 600;

        // teddy support
        Texture2D teddySprite;
        Texture2D explosionSprite;
        Texture2D mineSprite;
        TeddyBear teddy;

        // pickup support
        Texture2D pickupSprite;
        List<TeddyBear> bears;
        List<Explosion> explosions;
        List<Mine> mines;
        Random random;
        double previousSpawnTime = 0;
        

        // click processing
        bool rightClickStarted = false;
        bool rightButtonReleased = true;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // STUDENTS: set resolution and make mouse visible
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            this.IsMouseVisible = true;

            bears = new List<TeddyBear>();
            explosions = new List<Explosion>();
            mines = new List<Mine>();
            random = new Random();

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // STUDENTS: load teddy and pickup sprites

            teddySprite = this.Content.Load<Texture2D>("teddybear");
            mineSprite = this.Content.Load<Texture2D>("mine");
            explosionSprite = this.Content.Load<Texture2D>("explosion"); 


            // STUDENTS: create teddy object centered in window            

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            base.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            //    this.Exit();

            // STUDENTS: get current mouse state and update teddy            
            MouseState mouse = Mouse.GetState();
            previousSpawnTime += gameTime.ElapsedGameTime.Milliseconds;
            if (mouse.LeftButton == ButtonState.Pressed)
            {
                mines.Add(new Mine(mineSprite, mouse.X, mouse.Y));
            }
            double spawntime = 1000 + random.NextDouble() * 2000;
            if (previousSpawnTime > spawntime)
            {
                bears.Add(new TeddyBear(teddySprite, new Vector2((float)random.NextDouble(), (float)random.NextDouble()), WINDOW_WIDTH, WINDOW_HEIGHT));
                previousSpawnTime = 0;
            }

            foreach (TeddyBear bear in bears)
            {
                foreach (Mine mine in mines)
                {
                    if (bear.CollisionRectangle.Intersects(mine.CollisionRectangle))
                    {
                        explosions.Add(new Explosion(explosionSprite, mine.CollisionRectangle.X, mine.CollisionRectangle.Y));
                        bear.Active = false;
                        mine.Active = false;
                        
                    }
                }
                
            }

            foreach (TeddyBear bear in bears)
            {
                bear.Update(gameTime);
            }            

            foreach (Explosion explosion in explosions)
            {
                explosion.Update(gameTime);
            }

            bears = bears.Where(x => x.Active == true).ToList();
            mines = mines.Where(x => x.Active == true).ToList();
            explosions = explosions.Where(x => x.Playing == true).ToList();
            base.Update(gameTime);            
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // draw game objects
            spriteBatch.Begin();

            // STUDENTS: Uncomment the following line AFTER you create
            // a teddy object in the LoadContent method
            foreach (TeddyBear bear in bears)
            {
                bear.Draw(spriteBatch);
            }
            
            foreach (Mine mine in mines)
            {
                mine.Draw(spriteBatch);
            }

            foreach (Explosion explosion in explosions)
            {
                explosion.Draw(spriteBatch);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
